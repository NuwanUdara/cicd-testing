# cicd-testing
To build an image, push the image and deploy it in the cluster.
* Using a helm upgrade on the deployment.
* Docker needs certification to get access to docker.sock


### if not deploying give a cluster role binding.

```` bash
microk8s kubectl create clusterrolebinding gitlab-runner-binding --clusterrole=cluster-admin --serviceaccount=gitlab-runner:default
````
